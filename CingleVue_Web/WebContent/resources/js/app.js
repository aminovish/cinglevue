/**
 * Created by aminovish on 8/24/15.
 */
var myApp = angular.module('myApp', ['ngRoute','ui.bootstrap','ngTable','restangular']);

myApp.config(function($routeProvider,RestangularProvider){
    RestangularProvider.setBaseUrl('http://localhost:3000/CingleVue_Server/api/');
    $routeProvider.when('/', {templateUrl: 'pages/list.html', controller: 'SchoolCtrl'});
    $routeProvider.when('/new', {templateUrl: 'pages/new.html', controller: 'SchoolCtrl'});
    $routeProvider.when('/edit', {templateUrl: 'pages/new.html', controller: 'SchoolCtrl'});

});