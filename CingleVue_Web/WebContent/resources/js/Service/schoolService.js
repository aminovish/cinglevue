/**
 * Created by aminovish on 8/24/15.
 */

myApp.service('ScoolService' ,function (Restangular){

    var self = this;
    self.Schools = Restangular.all('schools');

    self.loadSchools = function(callback){
        self.Schools.getList().then(function (values)
        {
            callback(values);
        });
    }
    self.createSchool = function(callback,school){
        callback(self.Schools.post(school))
    }
    self.findSchoolById = function(callback,id){
        callback(self.Schools.get(id))
    }
    self.deleteSchool = function(school){
      school.remove();
    }
});