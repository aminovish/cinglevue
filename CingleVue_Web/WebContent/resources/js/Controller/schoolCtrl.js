/**
 * Created by aminovish on 8/24/15.
 */

myApp.controller('SchoolCtrl', function($scope,$routeParams,ngTableParams,$filter,ScoolService,$location,$rootScope) {

    $scope.schools = [];
    $scope.school = {};

    console.log($rootScope.id);
    $scope.edit = false;

    $scope.status = {
        opened: false
    };
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };


    /*
     * Load Table
     */

    $scope.schoolTable = new ngTableParams({
        page: 1,            // show first page
        count: 10,          // count per page
        sorting: {
            name: 'asc'     // initial sorting
        }
    }, {
        total:  $scope.schools.length, // length of data
        getData: function ($defer, params) {
            var orderedData = params.sorting() ?
                $filter('orderBy')( $scope.schools, params.orderBy()) :
                $scope.schools;
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
        }
    })

    $scope.loadSchools = function(){
        ScoolService.loadSchools(function(schools){
            $scope.schools = schools;
            $scope.schoolTable.reload();
        });
    }

    $scope.findSchoolById = function(){
        ScoolService.findSchoolById(function(school){
            $scope.school = school.$object;
            console.log($scope.school);
        },schoolId)
    }

    $scope.createSchool = function(){
        ScoolService.createSchool(function(school){
            $scope.loadSchools();
            $scope.schoolTable.reload();
            $rootScope.id = null;
        },$scope.school)
    }

    $scope.deleteSchool = function(id){
        var index = _.findIndex($scope.schools,{id:id});
        ScoolService.deleteSchool($scope.schools[index]);
        $scope.schools.splice(index,1);
        $scope.schoolTable.reload();
    }

    $scope.goToEdit = function(id){
        $rootScope.id = id;
        $location.path("/edit");
    }
    
    $scope.goToNew = function(){
        $rootScope.id = null;
        $location.path("/new");
    }
    $scope.open = function($event) {
        $scope.status.opened = true;
    };

    var schoolId = ($rootScope.id) ? $rootScope.id : null;
    if(schoolId != null){
        $scope.findSchoolById();
        $scope.edit = true;
    }
    $scope.loadSchools();
});