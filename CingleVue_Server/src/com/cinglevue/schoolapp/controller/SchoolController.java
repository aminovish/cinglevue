package com.cinglevue.schoolapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cinglevue.schoolapp.model.School;
import com.cinglevue.schoolapp.service.ISchoolService;

@Controller
public class SchoolController {

	@Autowired(required = true)
	@Qualifier("schoolService")
	ISchoolService schoolService;

	@RequestMapping(method = RequestMethod.GET, value = "/api/schools", headers = "Accept=application/json")
	public @ResponseBody List<School> findAll() {
		System.out.println("FindAll");
		return schoolService.findAll();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/api/schools", headers = "Accept=application/json")
	public @ResponseBody School createSchool(@RequestBody School school) {
		return schoolService.createOrUpdate(school);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/api/schools/{id}", headers = "Accept=application/json")
	public @ResponseBody School updateSchool(@RequestBody School school, @PathVariable String id) {
		school.setId(id);
		return schoolService.createOrUpdate(school);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/api/schools/{id}", headers = "Accept=application/json")
	public @ResponseBody School findSchoolById(@PathVariable String id) {
		return schoolService.findById(id);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/api/schools/{id}", headers = "Accept=application/json")
	public @ResponseBody List<School> deleteSchool(@PathVariable String id) {
		School sc =  schoolService.findById(id);
		schoolService.deleteSchool(sc);
		return schoolService.findAll(); 
	}

}
