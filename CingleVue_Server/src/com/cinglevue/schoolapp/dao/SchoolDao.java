package com.cinglevue.schoolapp.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.cinglevue.schoolapp.model.School;

@Repository
public class SchoolDao implements ISchoolDao {

	@Autowired
	MongoTemplate mongoTemplate;

	public SchoolDao() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public School createOrUpdate(School s) {
		// TODO Auto-generated method stub
		mongoTemplate.save(s);
		Query query = new Query();
		query.addCriteria(Criteria.where("name").is(s.getName()));
		return mongoTemplate.findOne(query, School.class);
	}

	@Override
	public School findById(String id) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		return mongoTemplate.findOne(query, School.class);
	}

	@Override
	public void deleteSchool(School s) {
		mongoTemplate.remove(s);

	}

	@Override
	public List<School> findAll() {
		// TODO Auto-generated method stub
		return mongoTemplate.findAll(School.class);
	}
}
