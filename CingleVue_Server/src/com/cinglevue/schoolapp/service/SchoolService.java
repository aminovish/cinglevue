package com.cinglevue.schoolapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.cinglevue.schoolapp.dao.ISchoolDao;
import com.cinglevue.schoolapp.model.School;

@Component
public class SchoolService implements ISchoolService {

	@Autowired(required = true)
	@Qualifier("schoolDao")
	ISchoolDao schoolDao;

	public SchoolService() {
		// TODO Auto-generated constructor stub
	}

	public School createOrUpdate(School s) {
		// TODO Auto-generated method stub
		return schoolDao.createOrUpdate(s);
	}

	public School findById(String id) {
		// TODO Auto-generated method stub
		return schoolDao.findById(id);
	}

	public void deleteSchool(School s) {
		schoolDao.deleteSchool(s);;
	}

	public List<School> findAll() {
		// TODO Auto-generated method stub
		return schoolDao.findAll();
	}

}
