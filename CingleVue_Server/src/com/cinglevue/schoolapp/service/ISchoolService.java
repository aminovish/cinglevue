package com.cinglevue.schoolapp.service;

import java.util.List;

import com.cinglevue.schoolapp.model.School;

public interface ISchoolService {

	public School createOrUpdate(School s);

	public School findById(String id);

	public void deleteSchool(School s);

	public List<School> findAll();
}
