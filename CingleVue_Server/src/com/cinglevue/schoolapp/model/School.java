package com.cinglevue.schoolapp.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "School")
public class School implements Serializable {

	private String id;
	private String name;
	private String address;
	private Long numberOfStudents;
	private String yearsOfCreation;
	private String phone;
	private Long numberOfTeachers;
	private Long numberOfClasses;
	private String urlImage;

	public School() {

	}

	public School(String name, String address, Long numberOfStudents, String yearsOfCreation, String phone,
			Long numberOfTeachers, Long numberOfClasses, String urlImage) {
		this.name = name;
		this.address = address;
		this.numberOfStudents = numberOfStudents;
		this.yearsOfCreation = yearsOfCreation;
		this.phone = phone;
		this.numberOfTeachers = numberOfTeachers;
		this.numberOfClasses = numberOfClasses;
		this.urlImage = urlImage;
	}

	@Id
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getNumberOfStudents() {
		return numberOfStudents;
	}

	public void setNumberOfStudents(Long numberOfStudents) {
		this.numberOfStudents = numberOfStudents;
	}

	public String getYearsOfCreation() {
		return yearsOfCreation;
	}

	public void setYearsOfCreation(String yearsOfCreation) {
		this.yearsOfCreation = yearsOfCreation;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getNumberOfTeachers() {
		return numberOfTeachers;
	}

	public void setNumberOfTeachers(Long numberOfTeachers) {
		this.numberOfTeachers = numberOfTeachers;
	}

	public Long getNumberOfClasses() {
		return numberOfClasses;
	}

	public void setNumberOfClasses(Long numberOfClasses) {
		this.numberOfClasses = numberOfClasses;
	}

	public String getUrlImage() {
		return urlImage;
	}

	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}

}